#include <iostream>
#include <iomanip>
#include <sstream>

using namespace std;

int 
main(int argc, char** argv) 
{
  typedef unsigned long long number_t;
  const size_t max_bits = sizeof(number_t) * 8;
  if (argc < 2) {
    cout << "Usage: " << argv[0] << " NUMBER" << endl
	 << "(maximum size in bits of NUMBER is " << max_bits << std::endl;
    return 0;
  }
  
  number_t number = 0;
  stringstream s(argv[1]);
  if (argv[1][1] == 'x' || argv[1][1] == 'X')
    s >> hex >> number;
  else if (argv[1][0] == '0') 
    s >> oct >> number;
  else if (argv[1][0] == 'b' || argv[1][0] == 'B') {
    std::string ss(&(argv[1][1]));
    number_t n = ss.size();
    if (n > max_bits) {
      cerr << "Maximum of " << max_bits << " bits" << endl;
      return 0;
    }
    for (number_t i = 0; i < n; i++) {
      number += (ss[i] == '0' ? 0 : (number_t(1) << (n - i - 1)));
      std::cout << ss[i];
    }
    std::cout << std::endl;
  }
  else 
    s >> dec >> number;
  
  cout << "Number is:\n"
       << "  (dec): " << setw(max_bits) << dec << number << endl
       << "  (hex): " << setw(max_bits) << hex << number << endl
       << "  (oct): " << setw(max_bits) << oct << number << endl
       << "  (bin): " << flush;
  
  for (int i = max_bits-1; i >= 0; i--) {
#if 0
    if (i == max_bits-1 && number < 0) {
      cout << 1 << flush;
      continue;
    }
#endif
    number_t b = ((number_t(1) << i) & number) >> i;
    cout << setw(1) << b << flush;
  }
  cout << endl << "         " << flush;
  for (int i = max_bits-1; i >= 0; i--) 
    if (i % 10 == 0)
      cout << setw(1) << dec << (i / 10) << flush;
    else 
      cout << " " << flush;
  cout << endl << "         " << flush;
  for (int i = max_bits-1; i >= 0; i--) 
    cout << setw(1) << dec << (i % 10) << flush;
  cout << endl;
  
  return  number;
}
